import javax.swing.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Vehiculo {
    public String marca;
    public Integer modelo;
    public BigDecimal precio;
    public String vehiculo2;
    public String getVehiculo3() {

        return vehiculo2;
    }
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getModelo() {
        return modelo;
    }

    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public static void main(String[] args) {
        List<Vehiculo> vehiculos = new ArrayList<>();
        boolean agregarVehiculo = true;

        while (agregarVehiculo) {
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.setMarca(JOptionPane.showInputDialog("Digite la marca del vehiculo"));
            System.out.println(vehiculo.getMarca());

            vehiculo.setModelo(Integer.valueOf(JOptionPane.showInputDialog("Digite el modelo del vehiculo")));
            System.out.println(vehiculo.getModelo());

            vehiculo.setPrecio(new BigDecimal(JOptionPane.showInputDialog("Digite el precio del vehiculo")));
            System.out.println(vehiculo.getPrecio());

            String tipoVehiculo = JOptionPane.showInputDialog("Que tipo de vehiculo es?");

            switch (tipoVehiculo) {
                case "auto":
                    auto puertas1 = new auto();
                    puertas1.setPuertasauto(Integer.valueOf(JOptionPane.showInputDialog("Ingrese el numero de puertas")));
                    System.out.println(puertas1.getPuertasauto());
                    break;

                case "furgoneta":
                    furgoneta capcarga1 = new furgoneta();
                    capcarga1.setCapcarga(Double.valueOf(JOptionPane.showInputDialog("Ingrese el volumen de carga en kg")));
                    System.out.println(capcarga1.getCapcarga() + " kg");
                    break;

                case "camion":
                    camion capcamion = new camion();
                    capcamion.setCapcamion(Double.valueOf(JOptionPane.showInputDialog("Ingrese la capacidad de carga en kg")));
                    System.out.println(capcamion.getCapcamion() + " kg");
                    break;
            }

            vehiculos.add(vehiculo);
            String respuesta = JOptionPane.showInputDialog("¿Desea agregar otro vehiculo, si/no?");
            agregarVehiculo = respuesta.equalsIgnoreCase("si");
        }

        String respuesta = JOptionPane.showInputDialog("¿Desea ver la lista de vehiculos ingresados, si/no?");
        if (respuesta.equalsIgnoreCase("si")) {
            for (Vehiculo vehiculo : vehiculos) {

                System.out.println("Tipo de Vehiculo: " + vehiculo.getVehiculo3() + ", Marca: " + vehiculo.getMarca() + ", Modelo: " + vehiculo.getModelo() + ", Precio: " + vehiculo.getPrecio());
            }
        }
    }
}